# wf-shadow

`wf-shadow` is a script written in order to fill the void in instant-replay software for Wayland, specifically wlroots compositors. It uses wf-recorder and wofi in order to accomplish the task.


# Usage

WARNING: Do NOT use Ctrl+C on a `wf-shadow -s` session.

```
USAGE: wf-shadow OPTION [OPTIONS...]

OPTIONS:
	-s, --start [OUTPUT]	Start a wf-shadow instance with output number OUTPUT.
	-r, --record [OUTPUT]	Record the last few moments of video using a wofi menu.
	-e, --end [OUTPUT]	End wf-shadow instance with output number OUTPUT.
	-a, --audio DEVICE	Use DEVICE for audio recording. Passed to wf-recorder -a.
	-V, --video DEVICE	Use DEVICE for video encoding. Passed to wf-recorder -d.
	-A, --acodec CODEC	Use CODEC as the audio codec in ffmpeg encoding.
	-c, --vcodec CODEC	Use CODEC as the video codec in ffmpeg encoding.
	-n, --no-audio	Disable audio recording. Overrides audio options.
	-N, --no-video	Disable video recording. Overrides video options.
	-y, --yes-audio	Enable audio recording. Overrides -n.
	-Y, --yes-video	Enable video recording. Overrides -N.
	-R, --raw-record OUTPUT FILE SECONDS	Low level record. Passed directly to the \`record\` function.
	-v, --verbose	Verbose output.
	-h, --help	Show this menu.
	--version	Display version information.

For a more in-depth guide, see wf-shadow(1)
```


# Dependencies

`wf-shadow` depends on `wf-recorder`, `wofi` (optional), `wlr-randr`, and, obviously, `bash`.

`wf-shadow-tui` depends on `dialog` alongside all of these programs.

# Installation

## Package Manager

### Arch-based

To install `wf-shadow` on Arch-based distributions, download and install the `wf-shadow` package from the AUR.

If you would like the latest, maybe less stable version, install `wf-shadow-git` from the AUR.

Both packages were made by d4ksh.

## Manual Installation

Put this script somewhere in your $PATH and set the executable bit with `chmod +x`. To find which directories are in your $PATH, run `sed 's/:/\n/g' <<< "$PATH"`.


# Contributing

## Writing issues

Make sure to state as many details relating to wf-shadow as possible when making an issue. These include, but are not limited to: version, dependency versions, and verbose (-v) output.


### Common issues

#### "A wf-shadow instance is already running on display X" but no wf-shadow is running.

End the nonexistent session with `wf-shadow -e X`.


## Merge requests

Make sure to always keep the style of code used in wf-shadow when requesting a merge (we don't want inconsistent or unreadable code!)
